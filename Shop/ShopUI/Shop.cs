﻿using ShopLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopUI
{
    public partial class Shop : Form
    {
        //create data at form level with Store class which contains list of vendors and items
        //Create store object
        private Store store = new Store();
        //create a list for shopping cart
        private List<Item> shoppingCartData = new List<Item>();
        //create binding source to connect data to design
        BindingSource itemsBinding = new BindingSource();
        BindingSource cartBinding = new BindingSource();
        BindingSource vendorsBinding = new BindingSource();
        private decimal storeProfit = 0;
        //auto developed constructor
        public Shop()
        {
            InitializeComponent();
            SetupData();
            //Links our items to the binding source
            itemsBinding.DataSource = store.Items.Where(x => x.Sold == false).ToList();
            //Links our items to the design list box
            itemsListBox.DataSource = itemsBinding;
            //give list box something to display
            itemsListBox.DisplayMember = "Display";
            itemsListBox.ValueMember = "Display";

            cartBinding.DataSource = shoppingCartData;
            shoppingCartListBox.DataSource = cartBinding;

            shoppingCartListBox.DisplayMember = "Display";
            shoppingCartListBox.ValueMember = "Display";

            vendorsBinding.DataSource = store.Vendors;
            vendorListBox.DataSource = vendorsBinding;

            vendorListBox.DisplayMember = "Display";
            vendorListBox.ValueMember = "Display";
        }

        //Method to create some dummy data for us to use.
        private void SetupData()
        {
            //Vendor demoVendor = new Vendor();
            //demoVendor.FirstName = "Stephen";
            //demoVendor.LastName = "Collins";
            //demoVendor.Commission = .5;

            //store.Vendors.Add(demoVendor);

            //demoVendor = new Vendor();
            //demoVendor.FirstName = "Stephanie";
            //demoVendor.LastName = "Scurlock";
            //demoVendor.Commission = .5;

            //store.Vendors.Add(demoVendor);

            store.Vendors.Add(new Vendor { FirstName = "Stephen", LastName = "Collins" });
            store.Vendors.Add(new Vendor { FirstName = "Stephanie", LastName = "Scurlock" });

            store.Items.Add(new Item
            {
                Title = "Moby Dick",
                Description = "Book about a whale",
                Price = 4.50M,
                Owner = store.Vendors[0]
            });
            store.Items.Add(new Item
            {
                Title = "A Tale of Two Cities",
                Description = "Book about a revolution",
                Price = 2.99M,
                Owner = store.Vendors[1]
            });
            store.Items.Add(new Item
            {
                Title = "Harry Potter Book 1",
                Description = "Book about a boy and wizardry",
                Price = 5.20M,
                Owner = store.Vendors[1]
            });
            store.Items.Add(new Item
            {
                Title = "Jane Eyre",
                Description = "Book about a girl",
                Price = 1.50M,
                Owner = store.Vendors[0]
            });

            store.Name = "Stephens Shop";
        }


        //auto developed
        private void label1_Click(object sender, EventArgs e)
        {

        }
        //auto developed
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void addToCart_Click(object sender, EventArgs e)
        {
            // Figure out what is selected from items list
            // copy that item to shopping cart
            // Do we remove the item from the items list? -no
            Item selectedItem = (Item)itemsListBox.SelectedItem;

            shoppingCartData.Add(selectedItem);

            cartBinding.ResetBindings(false);
        }

        private void makePurchase_Click(object sender, EventArgs e)
        {
            // mark each iem in the cart as sold
            // Clear the cart

            foreach (Item item in shoppingCartData)
            {
                item.Sold = true;
                item.Owner.PaymentDue += (decimal)item.Owner.Commission * item.Price;
                storeProfit += (1 - (decimal)item.Owner.Commission) * item.Price;
            }

            shoppingCartData.Clear();

            itemsBinding.DataSource = store.Items.Where(x => x.Sold == false).ToList();

            storeProfitValue.Text = string.Format("${0}", storeProfit);

            cartBinding.ResetBindings(false);
            itemsBinding.ResetBindings(false);
            vendorsBinding.ResetBindings(false);
        }
    }
}
